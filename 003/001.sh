#!/bin/bash

echo "Enter file name 1:"
read file1
echo "Enter file name 2:"
read file2
if [ $(wc -l < $file1) -gt $(wc -l < $file2) ]; then
	echo "$file1 has more lines."
elif [ $(wc -l < $file1) -lt $(wc -l < $file2) ]; then 
	echo "$file2 has more lines."
else 
	echo "$file1 and $file2 have an equal ammount of lines."
fi

