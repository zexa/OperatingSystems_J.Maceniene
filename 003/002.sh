#!/bin/bash
echo "Enter dir 1:"
read dir1
echo "Enter dir 2:"
read dir2
if [ $(ls $dir1 | wc -l) -gt $(ls $dir2 | wc -l) ]; then
	echo "$dir1 has more files."
elif [ $(ls $dir1 | wc -l) -lt $(ls $dir2 | wc -l) ]; then 
	echo "$dir2 has more files."
else
	echo "$dir1 and $dir2 have the same ammount of files."
fi
