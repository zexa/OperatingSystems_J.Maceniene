#!/bin/bash
# determine which folder is the bigget in the ammount of files

mostFiles=0
for i in $(ls); do
	if [ -d $i ]; then
		fileAmmount=$(find $i | wc -l)
		if [ $fileAmmount -gt $mostFiles ]; then
			mostFiles=$fileAmmount
			dirName=$i
		fi
	fi
done

echo "$dirName $mostFiles"
