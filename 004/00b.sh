#!/bin/bash
max=0
for i in $(ls); do
	currSize=$(stat -c %s $i)
	if [ $currSize -gt $max ];then
		max=$currSize
		biggestFile=$i
	fi
done

echo $biggestFile $max
