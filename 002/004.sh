#!/bin/bash
file=./data
output=./output
remove_line=5
head -n $(($remove_line - 1)) $file > $output
tail -n $(($(wc -l < $file) - $remove_line - 1)) $file >> $output

