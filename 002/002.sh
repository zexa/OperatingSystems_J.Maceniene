#!/bin/bash
# Write the script that outputs the n-th text line of the text file.
# $1 - file name, $2 - # line
echo $(head -n $2 $1 | tail -n 1)
