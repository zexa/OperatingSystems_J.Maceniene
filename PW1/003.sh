#!/bin/bash

# Write a script that prints out to screen the hidden non empty files of a selected directory


cd $1
hiddenFiles=$(ls -A | grep "^\.")

for i in $hiddenFiles; do
#	echo "$i"
	if [ -s $i ] && [ ! -d $i ]; then
		echo $i
	fi
done
