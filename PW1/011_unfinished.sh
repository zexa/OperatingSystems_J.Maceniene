#!/bin/bash

# Write script - address book with functions such as:
# * New record insertion (name, surname, phone no., email)
# * Record search
# * List of all records
# * Record removal
# * Record correction

# Prints dir of script
SOURCE="${BASH_SOURCE[0]}"
# Create db if there ain't one
if [ ! -e $SOURCE/011_db ]
	touch $SOURCE/011_db
fi

function getLastID {
	maxID=0
	while read -r line; do
		currID=$(echo "$line" | { read first _ ; echo $first ; })
		if [ $currID -gt $maxID ]; then
			maxID=$currID
		fi
	done < $SOURCE/011_db
	echo $maxID
}

case $1 in
	1)
		# New entry
		echo "Use commas to separate"
		echo "An ID for your entry will be generated automatically."
		echo "Name, surname, phone no., email"
		echo "example:"
		echo "Paulius, Donutis, 860810012, pauldonuts@eksdi.mail"
		read entry
		# generate ID here
		if [ -s $SOURCE/011_db ]; then
			entryID=0
		else
			lastID=$(getLastID)
			entryID=$((lastID + 1))
		fi
		echo "$entryID $entry" >> $SOURCE/011_db 	
	2)	
		# Search entry
		echo "Enter string to search for"
		read entry
		echo "ID Name, surname, phone no., email"
		grep "$entry" $SOURCE/011_db
	3)
		# Print DB
		echo "ID Name, surname, phone no., email"
		cat $SOURCE/011_db
	4)
		# Entry removal
		# NEED ID HERE
		echo "Enter ID of entry to remove"
		read entry
	5)
		# Entry correction
		# NEED ID HERE
		echo "Enter ID of entry to correct"
		read entry
	*)
		# Print menu
		echo "Use \$1 to make your choice"
		echo "1 - New record insertion (name, surname, phone no., email")
		echo "2 - Record search"
		echo "3 - List of all records"
		echo "4 - Record removal"
		echo "5 - Record correction" 
esac

