#!/bin/bash

# Write script that removes all empty files and directories in selected directory and prints out to screen the list of deleted items included type of item (i.e. directory or file) and last accessed date and time of item.

cd $1

echo "Removed files:"
for i in $(ls -A); do
	if [ -e $i ] && [ ! -s $i ] || [ ! "$(ls -A $i)" ]; then
		echo $(stat -c '%n %x' $i)
		rm -r $i
	fi
done
