#!/bin/bash

# Write script that prints out to screen only those files of selected directory, that has permission only „read“.
# Assuming "read" as in "read" for current user

cd $1

for file in $(ls -la); do
	if [ -r $file ] && [ ! -w $file ] && [ ! -x $file ]; then
		echo $file
	fi
done
