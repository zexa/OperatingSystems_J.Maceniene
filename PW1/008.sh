#!/bin/bash

# Write a script that prints out to screen the list of files of directory tree that are not less (more or equal) than selected size. Root of tree is selected freely.

# $1 is dir
# $2 is size in bytes

for file in $(ls $1); do
	if [ $(stat -c %B $file) -ge $2 ]; then
		echo $file
	fi
done
