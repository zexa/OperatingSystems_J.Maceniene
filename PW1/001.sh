#!/bin/bash

# Write a script that removes empty lines from text file
# so if a line is " " or "" remove line

cat $1 | grep '\S'
