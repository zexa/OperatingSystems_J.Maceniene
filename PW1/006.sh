#!/bin/bash

# Write script that removes all empty files in a whole directory tree. Root of tree is selected freely.

function recursive {
	cd $1

	for file in $(ls -A); do
		if [ ! -s $file ]; then
			echo "$(pwd)/$file"
			rm $file
		fi
		if [ -d $file ]; then
			recursive $file
		fi
	done
	
	cd ..
}

recursive $1
