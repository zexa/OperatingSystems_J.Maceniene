#!/bin/bash

# Write a script that finds the longest line in text file and prints out to screen the line number and the line itself.

max=0

while read p; do
	if [ ${#p} -gt $max ]; then
		#echo "works"
		max=${#p}
		maxl=$p
	fi
done < ./test
echo "$max $maxl"
