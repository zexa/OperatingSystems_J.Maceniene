#!/bin/bash

# Write script that prints out to screen the largest directory measured in ammount of simple files (included hidden ones) of selected directory 

cd $1
maxDirFiles=0
for dir in $(find $1 -type d); do 
	if [ $dir == "." ]; then
		continue
	fi
	currFiles=$(find $dir -type f | wc -l)
	if [ $currFiles -gt $maxDirFiles ]; then
		maxDirFiles=$currFiles
		maxDir=$dir
	fi
done

echo $maxDirFiles $maxDir
