#!/bin/bash

# Write a script that prints out to screen the list of files of selected directory that are accessed earlier than selected date.

# Creates file with $2 as its creation/modification/access time
# -t STAMP
# use [[CC]YY]MMDDhhmm[.ss] instead of current time  
touch -t $2 007_timestamp

# Finds all files that were accessed earlier than 007_timestamp
# -newerXY reference
# a   The access time of the file reference
# m   The modification time of the file reference
find . -not -neweram 007_timestamp 

rm 007_timestamp
